def wrong_revert(s: str) -> str:
    """Does not revert the input string correctly.
    
    This function will fail the tests. 
    
    Arguments:
        s {str} -- String input
    
    Returns:
        str -- Not the correct output. 
    """
    return s

def revert(s: str) -> str:
    """Reverts the input string
    
    Arguments:
        s {str} -- String input
    
    Returns:
        str -- The input string, reversed
    """
    return s[::-1]