import pytest
from hypothesis            import given
from hypothesis.strategies import text

from test_ci_cd            import reverse

def test_revert_single_use_case():
    single_use_case = "abcd"
    assert reverse.revert(single_use_case) == ''.join(reversed(single_use_case))

def test_wrong_revert_single_use_case():
    single_use_case = "abcd"
    assert reverse.wrong_revert(single_use_case) == ''.join(reversed(single_use_case))

@given(text())
def test_revert_general(input_string):
    assert reverse.revert(input_string) == ''.join(reversed(input_string))

@given(text())
def test_wrong_revert_general(input_string):
    assert reverse.wrong_revert(input_string) == ''.join(reversed(input_string))
